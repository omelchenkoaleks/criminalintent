package com.omelchenkoaleks.criminalintent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

public class CrimeFragment extends Fragment {

    private Crime mCrime;
    private EditText mTitleField;
    private Button mDateButton;
    private CheckBox mSolvedCheckBox;

    // экземпляр фрагмента настраивается в этом методе
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCrime = new Crime();
    }

    // в этом методе заполняется макет представления фрагмента,
    // а заполненный объект View возвращается активности-хосту
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // вызываем этом метод, чтобы явно заполнить представление макета фрагмента
        // третий параметр false потому-что представление будет добавлено
        // в коде активности, а не здесь
        View view = inflater.inflate(R.layout.fragment_crime, container, false);


        // ПОДКЛЮЧЕНИЕ ВИДЖЕТОВ во фрагменте

        // EditText
        mTitleField = view.findViewById(R.id.crime_title);

        // при назначении слушателя мы создаем анонимный класс,
        // который реализует интерфейс слушателя TextWatcher (его три метода)
        mTitleField.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence,
                                          int start, int count, int after) {
                // TODO:
            }

            @Override
            public void onTextChanged(CharSequence charSequence,
                                      int start, int before, int count) {
                // метод toString() на объекте charSequence возвращает
                // строку, которая используется для задания заголовка Crime
                mCrime.setTitle(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // TODO:
            }
        });

        // Button
        mDateButton = view.findViewById(R.id.crime_date);
        mDateButton.setText(mCrime.getDate().toString());
        // блокируем кнопку = что гарантирует,
        // что она никак не отреагирует на нажатие
        mDateButton.setEnabled(false);

        // CheckBox
        mSolvedCheckBox = view.findViewById(R.id.crime_solved);

        mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCrime.setSolved(isChecked);
            }
        });

        return view;
    }
}
