package com.omelchenkoaleks.criminalintent;

import java.util.Date;
import java.util.UUID;

public class Crime {

    // UUID вспомогательный класс Java, предоставляет
    // простой способ генерирования
    // универсально-уникальных идентификаторов
    private UUID mUUID;

    private String mTitle;
    private Date mDate;
    private boolean mSolved;

    public Crime() {
        // идентификатор UUID  в конструкторе
        // генерируется вызовом метода randomUUID()
        mUUID = UUID.randomUUID();

        // инициализация конструктором Date()
        // по умолчанию присваивает текущую дату
        mDate = new Date();
    }

    public UUID getUUID() {
        return mUUID;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public boolean isSolved() {
        return mSolved;
    }

    public void setSolved(boolean solved) {
        mSolved = solved;
    }
}
